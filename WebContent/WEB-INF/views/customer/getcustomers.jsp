<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>住户信息</title>
</head>
<body>
<div>
	<table>
		<c:forEach items="${customers }" var="a">
			<tr>
				<td>
					${a.customerId }
				</td>
				<td>
					${a.carPos }
				</td>
				<td>
					${a.roomNum }
				</td>
				<td>
					${a.customerName }
				</td>
				<td>
					${a.customerSex }
				</td>
				<td>
					${a.vipMark }
				</td>
				<td>
					${a.customerPhone }
				</td>
				<td>
					${a.customerCarId }
				</td>
				<td>
					<a href="/hotelmanagesystem/hotel/customer/delete?roomNum=${a.roomNum}">删除</a>
				</td>
				<td>
					<a href="/hotelmanagesystem/hotel/customer/update?customerId=${a.customerId} ">更新</a>
				</td>
			</tr>
		</c:forEach>	
	</table>
</div>
<br/>
<div>
	<a href="/hotelmanagesystem/hotel/customer/getbyroom">查询某房间入住人信息</a>
</div>
<br/>
<div>
	<a href="/hotelmanagesystem/hotel/customer/insert">添加入住人信息</a>
</div>
</body>
</html>