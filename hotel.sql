-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.5.60-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 hotel 的数据库结构
CREATE DATABASE IF NOT EXISTS `hotel` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `hotel`;

-- 导出  表 hotel.customer 结构
CREATE TABLE IF NOT EXISTS `customer` (
  `customerId` varchar(50) NOT NULL,
  `carPos` varchar(20) DEFAULT NULL,
  `roomNum` int(11) DEFAULT NULL,
  `customerName` varchar(50) NOT NULL,
  `customerSex` varchar(10) NOT NULL,
  `vipMark` tinyint(1) DEFAULT NULL,
  `customerPhone` varchar(20) DEFAULT NULL,
  `customerCarId` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`customerId`),
  KEY `FK_Relationship_2` (`carPos`),
  KEY `FK_Relationship_3` (`roomNum`),
  CONSTRAINT `FK_Relationship_2` FOREIGN KEY (`carPos`) REFERENCES `park` (`carPos`),
  CONSTRAINT `FK_Relationship_3` FOREIGN KEY (`roomNum`) REFERENCES `room` (`roomNum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 正在导出表  hotel.customer 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`customerId`, `carPos`, `roomNum`, `customerName`, `customerSex`, `vipMark`, `customerPhone`, `customerCarId`) VALUES
	('411413299810102222', '0004', 104, 'james', 'man', 0, '12345678801', '12233'),
	('411413299810102229', '0001', 101, 'harden', 'man', 1, '12345678801', '19999');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- 导出  表 hotel.employee 结构
CREATE TABLE IF NOT EXISTS `employee` (
  `empWorkid` varchar(50) NOT NULL,
  `empName` varchar(50) DEFAULT NULL,
  `empSex` varchar(5) DEFAULT NULL,
  `empId` varchar(50) DEFAULT NULL,
  `empPos` varchar(50) DEFAULT NULL,
  `empSer` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`empWorkid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 正在导出表  hotel.employee 的数据：~20 rows (大约)
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`empWorkid`, `empName`, `empSex`, `empId`, `empPos`, `empSer`) VALUES
	('001', 'wanghong', 'man', '7555335', 'waiter', 4000),
	('002', 'linyu', 'woman', '256773', 'waiter', 3500),
	('003', 'kongjie', 'man', '978257', 'waiter', 3500),
	('004', 'kewanyun', 'man', '3577642', 'waiter', 3500),
	('005', 'songtong', 'man', '4688765', 'waiter', 3500),
	('006', 'xuchu', 'man', '53674883', 'waiter', 3500),
	('007', 'caotanyun', 'woman', '12445322', 'waiter', 3500),
	('008', 'tanming', 'woman', '2367415', 'waiter', 3500),
	('009', 'zhangyu', 'man', '6754378', 'waiter', 3500),
	('010', 'fangzhou', 'man', '2356781', 'security', 3000),
	('011', 'suyun', 'woman', '2342234', 'security', 3000),
	('012', 'muyu', 'man', '2345522', 'security', 3000),
	('013', 'zoujian', 'man', '24316767', 'receptionist', 4000),
	('014', 'zoujia', 'man', '26237847', 'receptionist', 4000),
	('015', 'liaoyun', 'woman', '5689303', 'receptionist', 4000),
	('016', 'xuyichao', 'woman', '4788928', 'cleaner', 3000),
	('017', 'hejianqiang', 'man', '23465', 'cleaner', 3000),
	('018', 'taoyu', 'man', '3328997', 'manager', 7000),
	('019', 'liuyi', 'man', '231456', 'cook', 4000),
	('020', 'xiaoli', 'man', '1234567890', 'manager', 8000);
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;

-- 导出  表 hotel.manager 结构
CREATE TABLE IF NOT EXISTS `manager` (
  `userName` varchar(50) NOT NULL,
  `passWord` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 正在导出表  hotel.manager 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
INSERT INTO `manager` (`userName`, `passWord`) VALUES
	('wang', '123456'),
	('liu', '123456'),
	('www', '123456');
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;

-- 导出  表 hotel.park 结构
CREATE TABLE IF NOT EXISTS `park` (
  `carId` varchar(20) DEFAULT NULL,
  `carPos` varchar(20) NOT NULL,
  PRIMARY KEY (`carPos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 正在导出表  hotel.park 的数据：~20 rows (大约)
/*!40000 ALTER TABLE `park` DISABLE KEYS */;
INSERT INTO `park` (`carId`, `carPos`) VALUES
	('19999', '0001'),
	('1111', '0002'),
	('', '0003'),
	('', '0004'),
	(NULL, '0005'),
	(NULL, '0006'),
	(NULL, '0007'),
	(NULL, '0008'),
	(NULL, '0009'),
	(NULL, '0010'),
	(NULL, '0011'),
	(NULL, '0012'),
	(NULL, '0013'),
	(NULL, '0014'),
	(NULL, '0015'),
	(NULL, '0016'),
	(NULL, '0017'),
	(NULL, '0018'),
	(NULL, '0019'),
	(NULL, '0020');
/*!40000 ALTER TABLE `park` ENABLE KEYS */;

-- 导出  表 hotel.room 结构
CREATE TABLE IF NOT EXISTS `room` (
  `roomNum` int(11) NOT NULL,
  `roomState` tinyint(1) DEFAULT NULL,
  `roomMold` varchar(20) DEFAULT NULL,
  `roomCost` int(10) NOT NULL,
  PRIMARY KEY (`roomNum`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 正在导出表  hotel.room 的数据：~24 rows (大约)
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` (`roomNum`, `roomState`, `roomMold`, `roomCost`) VALUES
	(101, 1, 'SingleRoom', 190),
	(102, 1, 'SingleRoom', 190),
	(103, 0, 'SingleRoom', 190),
	(104, 0, 'DoubleRoom', 250),
	(105, 0, 'DoubleRoom', 250),
	(106, 0, 'DoubleRoom', 250),
	(107, 0, 'BigBedRoom', 200),
	(108, 0, 'BigBedRoom', 200),
	(109, 0, 'BigBedRoom', 200),
	(201, 0, 'SingleRoom', 190),
	(202, 0, 'SingleRoom', 190),
	(203, 0, 'SingleRoom', 190),
	(204, 0, 'DoubleRoom', 250),
	(205, 0, 'DoubleRoom', 250),
	(206, 0, 'DoubleRoom', 250),
	(207, 0, 'DoubleRoom', 200),
	(208, 0, 'DoubleRoom', 200),
	(301, 0, 'SingleRoom', 190),
	(302, 0, 'SingleRoom', 190),
	(303, 0, 'SingleRoom', 190),
	(305, 0, 'DoubleRoom', 250),
	(306, 0, 'DoubleRoom', 250),
	(307, 0, 'DoubleRoom', 250),
	(308, 0, 'BigBedRoom', 200);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
