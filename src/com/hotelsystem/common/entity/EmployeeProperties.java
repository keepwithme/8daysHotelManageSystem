package com.hotelsystem.common.entity;

public class EmployeeProperties {
    private String empWorkid;
    private String empName;
    private String empSex;
    private String empId;
    private String empPos;
    private String empSer;
	public String getEmpWorkid() {
		return empWorkid;
	}
	public void setEmpWorkid(String empWorkid) {
		this.empWorkid = empWorkid;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpSex() {
		return empSex;
	}
	public void setEmpSex(String empSex) {
		this.empSex = empSex;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpPos() {
		return empPos;
	}
	public void setEmpPos(String empPos) {
		this.empPos = empPos;
	}
	public String getEmpSer() {
		return empSer;
	}
	public void setEmpSer(String empSer) {
		this.empSer = empSer;
	}
	
    
}
