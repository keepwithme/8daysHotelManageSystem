package com.hotelsystem.customer.service;

import java.sql.SQLException;
import java.util.List;

import com.hotelsystem.common.entity.Customer;
import com.hotelsystem.common.entity.Park;
import com.hotelsystem.common.entity.Room;
import com.hotelsystem.customer.dao.CustomerDao;


public class CustomerService {
	private CustomerDao cd=new CustomerDao();
	
	public List<Customer> getCustomers(){
		return cd.findAll();
	}
	
	public List<Customer> getCustomersByRoomNum(int roomnum){
		return cd.findByRoomNum(roomnum);
	}
	
	public int deleteCustomers(int roomnum){
		return cd.delete(roomnum);
	}
	
	public int insertCustomers(Customer cus){
		return cd.insert(cus);
	}
	
	public int updateCustomers(Customer cus,String cusId){
		return cd.update(cus,cusId);
	}
	
	public Customer getCustomerById(String cusId){
		return cd.findByCustomerId(cusId);
	}
	
	public int updateRoomStateByCus(Room room){
		return cd.updateRoomState(room);
	}
	
	public int updateCarIdByCus(Park park){
		return cd.updateCarId(park);
	}
}
