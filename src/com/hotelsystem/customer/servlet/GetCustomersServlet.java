package com.hotelsystem.customer.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Customer;
import com.hotelsystem.customer.service.CustomerService;

@WebServlet("/hotel/customer/getall")
public class GetCustomersServlet extends HttpServlet{
	
	private CustomerService customerService=new CustomerService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		List<Customer> customers=customerService.getCustomers();
		
//		PrintWriter out=resp.getWriter();
//		out.println("hello");
		
		req.setAttribute("customers", customers);
		req.getRequestDispatcher("/WEB-INF/views/main/index.jsp").forward(req, resp);
	}
	
	
}
