package com.hotelsystem.employee.service;

import java.sql.SQLException;
import java.util.List;

import com.hotelsystem.common.entity.EmployeeProperties;
import com.hotelsystem.employee.dao.EmployeeDao;

public class EmployeeService {
	private EmployeeDao allemployee=new EmployeeDao();
  public List<EmployeeProperties> allQuery(){
	  return allemployee.AllQuery();
  }
  public int deleteEmployee(String empWorkid) throws SQLException{
	return allemployee.DeleteEmployee(empWorkid);  
  }
  public int insertEmployee(EmployeeProperties employeeProperties) throws SQLException{
	 return  allemployee.InsertEmployee(employeeProperties);
	 
  }
  public void updateEmployee(EmployeeProperties employeeProperties) throws SQLException{
	 allemployee.UpdateEmployee(employeeProperties);
  }
  public List<EmployeeProperties> singalEmployee(String empworkid) throws SQLException{
	return allemployee.SingalQuery(empworkid);
	  
  }
}
