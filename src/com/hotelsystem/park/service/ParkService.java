package com.hotelsystem.park.service;

import java.sql.SQLException;
import java.util.List;

import com.hotelsystem.common.entity.Park;
import com.hotelsystem.park.dao.ParkDao;



public class ParkService {
	
	private  ParkDao parkDao = new ParkDao();
	
	public List<Park> getCars(){
		
		return parkDao.findAll();
	}
	
	
	public Park getCarById( String carPos ){
		
		return parkDao.findById(carPos);
	}
	public void  updateCarStatus(Park park) throws SQLException{
		 parkDao.updateCar(park);
	}

}
