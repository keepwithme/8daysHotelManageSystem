package com.hotelsystem.park.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Park;
import com.hotelsystem.park.service.ParkService;
@WebServlet("/hotel/park")
public class ParkSearchServlet extends HttpServlet{

	private ParkService parkService=new ParkService();
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	    List<Park> park=parkService.getCars();
	    req.setAttribute("park",park);
		req.getRequestDispatcher("/WEB-INF/views/main/index.jsp").forward(req, resp);
	}
  
}
