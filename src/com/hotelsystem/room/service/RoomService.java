package com.hotelsystem.room.service;

import java.util.List;

import com.hotelsystem.common.entity.Room;
import com.hotelsystem.room.dao.RoomDao;


public class RoomService {
	
	private RoomDao roomDao = new RoomDao();
	
	public List<Room> getRooms(){
		
		return roomDao.findAll();
	}
	
	public int remove( int room_num ){
		try {
			//DBUtil.beginTransaction();
			return roomDao.delete(room_num);
			
			
			//DBUtil.commitTransaction();
		} catch (Exception e) {
			// TODO: handle exception
			//DBUtil.rollbackTransaction();
		}
		return -1;
	}
	
	public Room getRoomById( int room_num ){
		
		return roomDao.findById(room_num);
	}

	
	public List<Room> getEmptyRooms(){
		
		return roomDao.findEmpty();
	}
}
