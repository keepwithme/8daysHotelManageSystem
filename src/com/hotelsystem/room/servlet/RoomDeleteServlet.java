package com.hotelsystem.room.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.room.service.RoomService;



@WebServlet("/hotel/room/delete")
public class RoomDeleteServlet extends HttpServlet {
	
	private RoomService roomService = new RoomService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String roomNum = req.getParameter("roomNum");
		
		roomService.remove( Integer.parseInt(roomNum) );
		
		
		resp.sendRedirect("/hotelmanagesystem/hotel/index");
	}
	
	

}
