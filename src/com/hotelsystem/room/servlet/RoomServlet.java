package com.hotelsystem.room.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hotelsystem.common.entity.Room;
import com.hotelsystem.room.service.RoomService;



@WebServlet("/hotel/room")
public class RoomServlet extends HttpServlet {
	
	private RoomService roomService = new RoomService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String roomNum = req.getParameter("roomNum");
		
		Room room = roomService.getRoomById( Integer.parseInt(roomNum));
		
		req.setAttribute("room", room);
		
		req.getRequestDispatcher("/WEB-INF/views/room/update.jsp").forward(req, resp);
	}

	
}
